package com.newyearresolutionscalendar2021;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.card.MaterialCardView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;



public class ReminderActivity extends AppCompatActivity {
    SQLiteDatabase db;
    DatabaseHelper mDbHelper;
    SharedPreferences sharedPreferences;
    Button taskcomplete;
    Boolean click=true;
    TextView goalname,goaltime,goaldate;
//    MCalendarView view;
    String taskdate;
    long time;
    String totaltime;
    Button remove;
    MainAdpter mainAdpter;
    String name,tdate;
    int id=0;
    AdManager adManager;
    String cardname,cardtime,cardate;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
      //  adManager = new AdManager(this);
        taskcomplete=findViewById(R.id.taskcomplete);
        goalname=findViewById(R.id.goalname);
        goaltime=findViewById(R.id.goaltime);
        goaldate=findViewById(R.id.goaldate);
        remove=findViewById(R.id.toDoReminderRemoveButton);


        mDbHelper = new DatabaseHelper(this);
        db = mDbHelper.getWritableDatabase();
//        view = ((MCalendarView) findViewById(R.id.calendar_exp));
//        view.markDate(new DateData(2016, 3, 1).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, Color.GREEN)));
       // view.setDateCell(R.layout.layout_date_cell);
        taskcomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ReminderActivity.this, "Task Completed", Toast.LENGTH_SHORT).show();
               // adManager.showInterstitialAd(getApplicationContext());
            }
        });





          Intent intent=getIntent();
          name= intent.getStringExtra("names");
          id= intent.getIntExtra("notify",0);
          tdate= intent.getStringExtra("datesall");
         long ttime=intent.getLongExtra("ttimes",0);

         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
         Date date1 = new Date(ttime);
         totaltime = simpleDateFormat.format(date1);
         Log.i("alllnoti",name+id+tdate+ttime);
         int hghh=id;
         Log.i("AAAA", String.valueOf(hghh));

        goalname.setText(name);
        goaltime.setText(totaltime);
        goaldate.setText(tdate);


//        cardname=getIntent().getStringExtra("title");
//        cardate=getIntent().getStringExtra("date");
//        cardtime=getIntent().getStringExtra("time");
//        goalname.setText(cardname);
//        goaltime.setText(cardtime);
//        goaldate.setText(cardate);



//        Log.i("himani11", String.valueOf(id)+""+name+tdate+totaltime);


//        mDbHelper = new DatabaseHelper(this);
//        Boolean checkinsertdata = mDbHelper.insertstatusdata(name, totaltime, tdate);
//        if (checkinsertdata == true) {
//           // Log.i("himani", String.valueOf(time));
//           // Toast.makeText(context, "New Entry Inserted", Toast.LENGTH_SHORT).show();
//        } else {
//            // Toast.makeText(context, "New Entry no insertedt", Toast.LENGTH_SHORT).show();
//        }


        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.C_ID + "=" + id, null);
                Toast.makeText(ReminderActivity.this, "DELETED SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                finish();

               // adManager.showInterstitialAd(getApplicationContext());
                //  db.close();





//                try{
//                    if (id>0){
//                        Log.i("jkjj", String.valueOf(id));
//                        mDbHelper.deletedata(id);
//                        Toast.makeText(ReminderActivity.this,"DELETED SUCCESSFULLY",Toast.LENGTH_SHORT).show();
//                    }

//                }catch (Exception e){
//                    Log.i("saaa", e.getMessage());

//                }


            }
        });



//        view.setOnMonthChangeListener(new OnMonthChangeListener() {
//            @Override
//            public void onMonthChange(int year, int month) {
//                Toast.makeText(ReminderActivity.this, String.format("%d-%d", year, month), Toast.LENGTH_SHORT).show();
//            }
//        });
//        view.setOnDateClickListener(new OnDateClickListener() {
//            @Override
//            public void onDateClick(View view, DateData date) {
//
//                Toast.makeText(ReminderActivity.this, String.format("%d-%d", date.getMonth(), date.getDay(),date.getYear()), Toast.LENGTH_SHORT).show();
//
//                view.setBackgroundColor(Color.BLUE);
//
//
//            }
//        });

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i("geetaa",intent.getStringExtra("todo"));
    }
}