package com.newyearresolutionscalendar2021;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.UUID;

public class AlarmService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager alarmNotificationManager;
    public static final String TODOTEXT = "com.avjindersekhon.todonotificationservicetext";
    public static final String TODOUUID = "com.avjindersekhon.todonotificationserviceuuid";
    private String mTodoText;
    private UUID mTodoUUID;
    private Context mContext;

    public AlarmService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

  int notificationid=intent.getIntExtra("notificationid",0);
  String message=intent.getStringExtra("todo");
  int t=intent.getIntExtra("time",0);

      Intent mainIntent = new Intent(this, HealthActivity.class);
      PendingIntent pendingIntent=PendingIntent.getActivity(getApplicationContext(),0,mainIntent,0);
      NotificationManager manager=(NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder notification = new Notification.Builder(this)
                .setContentTitle(message)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setWhen(t)
                .setContentIntent(pendingIntent);


        manager.notify(notificationid, notification.build());

//        mTodoText = intent.getStringExtra(TODOTEXT);
//        mTodoUUID = (UUID) intent.getSerializableExtra(TODOUUID);
//
//        Log.d("OskarSchindler", "onHandleIntent called");
//        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        Intent i = new Intent(this, HealthActivity.class);
//        i.putExtra(AlarmService.TODOUUID, mTodoUUID);
//        Intent deleteIntent = new Intent(this, DeleteNotificationService.class);
//        deleteIntent.putExtra(TODOUUID, mTodoUUID);
//        Notification notification = new Notification.Builder(this)
//                .setContentTitle(mTodoText)
//                .setSmallIcon(R.drawable.ic_launcher_background)
//                .setAutoCancel(true)
//                .setDefaults(Notification.DEFAULT_SOUND)
//                .setDeleteIntent(PendingIntent.getService(this, mTodoUUID.hashCode(), deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT))
//                .setContentIntent(PendingIntent.getActivity(this, mTodoUUID.hashCode(), i, PendingIntent.FLAG_UPDATE_CURRENT))
//                .build();
//
//        manager.notify(100, notification);
    }



}
