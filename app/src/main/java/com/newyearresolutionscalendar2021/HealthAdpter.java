package com.newyearresolutionscalendar2021;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;

public class HealthAdpter extends RecyclerView.Adapter<HealthAdpter.ViewHolder> {
    private MyListData[] listdata;
    ImageView calenderimg, timer;
    EditText edttime, edtcalender, edttxt;
    Button save;
    Calendar calNow;
    int notificationid = 1;
    Context context;
    long time;
    SQLiteDatabase db;
    DatabaseHelper mDbHelper;
    // RecyclerView recyclerView;
    String goaltask, calenderdate;
    String caltime;


    public HealthAdpter(MyListData[] listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MyListData myListData = listdata[position];
        holder.smokingquittext.setText(listdata[position].getDescription());
//        holder.imageView.setImageResource(listdata[position].getImgId());
        holder.adoptlv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(view.getContext(), "click on item: " + myListData.getDescription(), Toast.LENGTH_LONG).show();

                final Dialog settingsDialog = new Dialog(context);
                LayoutInflater layoutInflater1 = LayoutInflater.from(view.getContext());
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                settingsDialog.setContentView(layoutInflater1.inflate(R.layout.dialogbox, null));
                settingsDialog.show();
                calenderimg = settingsDialog.findViewById(R.id.calenderimg);
                timer = settingsDialog.findViewById(R.id.timer);
                edttime = settingsDialog.findViewById(R.id.edttime);
                save = settingsDialog.findViewById(R.id.save);
                edtcalender = settingsDialog.findViewById(R.id.edtcalender);
                edttxt = settingsDialog.findViewById(R.id.edttxt);
                edttxt.setText(myListData.getDescription());
                goaltask = edttxt.getText().toString();


                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDbHelper = new DatabaseHelper(context);
                        long checkinsertdata = mDbHelper.insertQuery(goaltask, calenderdate, caltime);
                        if (checkinsertdata != -1) {
                            notificationid=(int)checkinsertdata;
                            setAlarm(time);
                          //  Log.i("himani", String.valueOf(time));
                           Toast.makeText(context, "set goal successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            // Toast.makeText(context, "New Entry no insertedt", Toast.LENGTH_SHORT).show();
                        }
                        settingsDialog.dismiss();

                    }
                });


                calenderimg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toast.makeText(HealthActivity.this, "hello", Toast.LENGTH_SHORT).show();
                        Calendar mcurrentDate = Calendar.getInstance();
                        int mYear = mcurrentDate.get(Calendar.YEAR);
                        int mMonth = mcurrentDate.get(Calendar.MONTH);
                        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                // TODO Auto-generated method stub
                                /*      Your code   to get date and time   */
                                selectedmonth = selectedmonth + 1;
                                // eReminderDate.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);

                                edtcalender.setText(selectedday + "/" + selectedmonth + "/" + selectedyear);
                                calenderdate = edtcalender.getText().toString();

                            }
                        }, mYear, mMonth, mDay);
                        mDatePicker.setTitle("Select Date");
                        mDatePicker.show();
                    }
                });


                timer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                calNow = Calendar.getInstance();
                                // Calendar calSet = (Calendar) calNow.clone();
                                calNow.set(Calendar.HOUR_OF_DAY, selectedHour);
                                calNow.set(Calendar.MINUTE, selectedMinute);
                                calNow.set(Calendar.SECOND, 0);
                                calNow.set(Calendar.MILLISECOND, 0);
                                time = calNow.getTimeInMillis();
                                if (calNow.compareTo(calNow) <= 0) {

                                    //Today Set time passed, count to tomorrow
                                    calNow.add(Calendar.DATE, 1);

                                }
                                //  edttime.setText(selectedHour+":"+selectedMinute+"");


                                if (selectedHour > 12) {
                                    edttime.setText(String.valueOf(selectedHour - 12) + ":" + (String.valueOf(selectedMinute) + "PM"));
                                } else if (selectedHour == 12) {
                                    edttime.setText("12" + ":" + (String.valueOf(selectedMinute) + "PM"));
                                } else if (selectedHour < 12) {
                                    if (selectedHour != 0) {
                                        edttime.setText(String.valueOf(selectedHour) + ":" + (String.valueOf(selectedMinute) + "AM"));
                                    } else {
                                        edttime.setText("12" + ":" + (String.valueOf(selectedMinute) + "AM"));
                                    }
                                }

                               // setAlarm(time);
                                caltime = edttime.getText().toString();

                            }
                        }, hour, minute, true);
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });


            }

        });
    }


    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView smokingquittext;
        public LinearLayout adoptlv;

        public ViewHolder(View itemView) {
            super(itemView);
            smokingquittext = itemView.findViewById(R.id.smokingquittext);
            adoptlv = itemView.findViewById(R.id.adoptlv);
        }
    }

    private void setAlarm(long calNow) {
        Intent notificationIntent = new Intent(context, MyReceiver.class);
        notificationIntent.putExtra("notificationId", notificationid);
        notificationIntent.putExtra("todo", edttxt.getText().toString());
        notificationIntent.putExtra("time", calNow);
        notificationIntent.putExtra("dates", calenderdate);
         PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
          // am.set(AlarmManager.RTC_WAKEUP, calNow, pendingIntent);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calNow, 7 * 24 * 60 * 60 * 1000, pendingIntent);

//        ArrayList<DataModel> minutes = new ArrayList<>();
//        for (int i =0;i<minutes.size();i++){a
//        }


//        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();
//
//        for (int i = 0; i < 10; ++i) {
//            Intent intent = new Intent(context, MyReceiver.class);
//            // Loop counter `i` is used as a `requestCode`
//             PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//            // Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)
//            am.set(AlarmManager.RTC_WAKEUP,
//                    calNow,
//                    pendingIntent);

//            intentArray.add(pendingIntent);



    }
}
