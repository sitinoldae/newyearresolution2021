package com.newyearresolutionscalendar2021;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "remindtable";
    public static final String TABLE_NAME = "tasksreminder";
    public static final String C_ID = "_id";
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    public static final String STATUS = "status";
    public static final String TIME = "time";
    public static final String DATE = "date";
    public static final int VERSION = 1;

    private final String createDB = "create table if not exists " + TABLE_NAME + " ( "
            + C_ID + " integer primary key autoincrement, "
            + TITLE + " text, "
            + STATUS + "boolean,"
            + TIME + " text, "
            + DATE + " text)";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createDB);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table " + TABLE_NAME);

    }

    public boolean insertdata(String title, String time, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, title);
        contentValues.put(TIME, time);
        contentValues.put(DATE, date);

        long id=db.insert(TABLE_NAME, null, contentValues);
        return true;
    }
    public long insertQuery(String title, String time, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, title);
        contentValues.put(TIME, time);
        contentValues.put(DATE, date);

        long id=db.insert(TABLE_NAME, null, contentValues);
        return id;
    }

    public boolean insertstatusdata(String title, String time, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, title);
        contentValues.put(TIME, time);
        contentValues.put(DATE, date);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getdata() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        return cursor;
    }


    public void deletedata (int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, C_ID +" = "+id,  null );

    }

        ArrayList<DataModel> listtime() {
        String sql = "SELECT * FROM "+ TABLE_NAME ;
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<DataModel> storelist = new ArrayList<>();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {

                String title = cursor.getString(cursor.getColumnIndex(TITLE));
                String text = cursor.getString(cursor.getColumnIndex(TIME));
                String time = cursor.getString(cursor.getColumnIndex(DATE));
                storelist.add(new DataModel( title, text,time));

            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return storelist;
    }
}
