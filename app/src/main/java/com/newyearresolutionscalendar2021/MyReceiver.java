package com.newyearresolutionscalendar2021;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static androidx.legacy.content.WakefulBroadcastReceiver.startWakefulService;

public class MyReceiver extends BroadcastReceiver {
    public static final String NOTIFICATION_ID = "111";
    public static final String NOTIFICATION = "";
    private static int MID = 1;
    public static final String CHANNELID = "channelID";
    private static final int NOTIF_ID = 1;

   SharedPreferences sharedPreferences;
    String dates;
    String message;
    long t;

    int notificationid;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
      //  Toast.makeText(context, "receiverstart", Toast.LENGTH_SHORT).show();

         notificationid=intent.getIntExtra("notificationId",0);
         message=intent.getStringExtra("todo");
         dates=intent.getStringExtra("dates");
         t=intent.getLongExtra("time",0);
        Log.i("himoo",notificationid+""+message+""+t+dates);

        Intent mainIntent = new Intent(context, ReminderActivity.class);
        mainIntent.putExtra("notify",notificationid);
        mainIntent.putExtra("names",message);
        mainIntent.putExtra("datesall",dates);
        mainIntent.putExtra("ttimes",t);
        PendingIntent pendingIntent=PendingIntent.getActivity(context,0,mainIntent,PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager manager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            CharSequence channelname="My Notification";
            int important=NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel= new NotificationChannel(CHANNELID,channelname,important);
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder=new NotificationCompat.Builder(context,CHANNELID)
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle("Alarm!")
                        .setContentText(message)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setWhen(t)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent);

        manager.notify(notificationid,builder.build() );


    }
}


