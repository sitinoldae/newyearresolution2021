package com.newyearresolutionscalendar2021;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class  MainAdpter  extends RecyclerView.Adapter<MainAdpter.ViewHolder> {

    private ArrayList<DataModel> list;
    Context context;
    Drawable icon;
    private Boolean isInfo = true;
    SharedPreferences sharedPreferences;

    public void setInfo(Boolean info) {
        isInfo = info;
    }

//    public void setList(ArrayList<Notifiylist> list) {
//        this.notify = list;
//    }
public void setList(ArrayList<DataModel> second) {
    this.list = second;
}

    public MainAdpter(Context context, ArrayList<DataModel> list) {
        this.list = list;
        this.context = context;
    }


    public ArrayList<DataModel> getList() {
        return list;
    }

    @Override
    public MainAdpter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.mainitems_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MainAdpter.ViewHolder holder, final int position) {
        final DataModel datalist = list.get(position);
        holder.time.setText(datalist.getTime());
        holder.title.setText(datalist.getTitle());
        holder.date.setText(datalist.getDate());
//        holder.lifebeautifulcard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent =new Intent(context,ReminderActivity.class);
//                intent.putExtra("notify",datalist.getId());
//                intent.putExtra("names",datalist.getTitle());
//                intent.putExtra("datesall",datalist.getDate());
//                intent.putExtra("ttimes",datalist.getTime());
//                context.startActivity(intent);
//
//            }
//        });

//        holder.lifebeautifulcard.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                ((Callback) context).onRowClicked(datalist.getId(), true);
//                return true;
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, text, time, date;
        ImageView imageInListView;
        RelativeLayout rv1layout;
        CheckBox check;
        View view;
        CardView lifebeautifulcard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
           // text = itemView.findViewById(R.id.text);
            time = itemView.findViewById(R.id.time);
            imageInListView = itemView.findViewById(R.id.app_icon);
            rv1layout = itemView.findViewById(R.id.rv1layout);
            check = itemView.findViewById(R.id.check);
            date = itemView.findViewById(R.id.date);
            lifebeautifulcard = itemView.findViewById(R.id.lifebeautifulcard);
            view = itemView;
        }
    }

    public interface Callback {
        void onRowClicked(String id, boolean b);
    }
}
