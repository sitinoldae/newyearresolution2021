package com.newyearresolutionscalendar2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.vo.DateData;

public class DateActivity extends AppCompatActivity {
    MCalendarView view;
    SharedPreferences sharedPreferences;
     String titlename;
     int mYear,mMonth,mDay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);
        view = ((MCalendarView) findViewById(R.id.calendar_exp));
       // view.markDate(new DateData(2020, 12, 3).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, Color.GREEN)));
        sharedPreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
         titlename=sharedPreferences.getString("date","");
        Log.i("hghg",titlename);


//        String strThatDay = "2012/11/27";
       // String dateString = "20/12/2018";
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//
//        Date readDate = null;
//        try {
//            readDate = df.parse(titlename);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(readDate.getTime());
//
//        Log.d("TAG", "Year: "+cal.get(Calendar.YEAR));
//        Log.d("TAG", "Month: "+cal.get(Calendar.MONTH));
//        Log.d("TAG", "Day: "+cal.get(Calendar.DAY_OF_MONTH));
////        mYear = thatDay.get(Calendar.YEAR);
////        mMonth = thatDay.get(Calendar.MONTH);
////        mDay = thatDay.get(Calendar.DAY_OF_MONTH);
//        Log.i("himani123",mYear+""+mMonth+""+mDay);


        view.markDate(new DateData(2020,12,5).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, Color.BLUE)));
        view.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {

                Toast.makeText(DateActivity.this, String.format("%d-%d", date.getMonth(), date.getDay(),date.getYear()), Toast.LENGTH_SHORT).show();

                if (titlename.equalsIgnoreCase("")){
                    view.setBackgroundColor(Color.BLUE);

                }
              //  getdate.setBackgroundResource(R.color.white);
            }
        });
    }
}