package com.newyearresolutionscalendar2021;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class GoalsActivity extends AppCompatActivity {
    Toolbar toolbar;
    ActionBar actionBar;
    ImageView backarrow,toolbar_save;
    Context context;
    TextView resulationtext;
    LinearLayout health,learning,financial,sociallv;
   AdView adView;
   Adds adManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goals);
        adManager = new Adds(this);
        toolbar=findViewById(R.id.toolbar);
        health=findViewById(R.id.health);
        learning=findViewById(R.id.learning);
        financial=findViewById(R.id.financial);
        sociallv=findViewById(R.id.sociallv);
        toolbar_save=toolbar.findViewById(R.id.toolbar_save);
        backarrow=toolbar.findViewById(R.id.back);
        resulationtext=toolbar.findViewById(R.id.resulationtext);
        toolbar_save.setVisibility(View.GONE);
        backarrow.setVisibility(View.VISIBLE);
        resulationtext.setVisibility(View.GONE);
        toolbar.setTitle("Select Categories");

        adView = findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        health.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // adManager.showInterstitialAd(getApplicationContext());
                Intent intent=new Intent(GoalsActivity.this,HealthActivity.class);
                startActivity(intent);

            }
        });




        learning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // adManager.showInterstitial();
                Intent intent=new Intent(GoalsActivity.this,LearningActivity.class);
                startActivity(intent);
            }
        });

        financial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // adManager.showInterstitialAd(getApplicationContext());
                Intent intent=new Intent(GoalsActivity.this,FinancialActivity.class);
                startActivity(intent);
            }
        });
        sociallv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             //   adManager.showInterstitial();
                Intent intent=new Intent(GoalsActivity.this,SocialActivity.class);
                startActivity(intent);
            }
        });
    }
}