package com.newyearresolutionscalendar2021;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,MainAdpter.Callback {
    DrawerLayout drawer;
    Toolbar toolbaar;
    CardView lifebeautiful;
    ImageView dazeicon;
    private ArrayList<DataModel> list;

    ImageView humbergunicon;
    ActionBarDrawerToggle actionBarDrawerToggle;
    FloatingActionButton floatingActionButton;
    LinearLayout healthlenarlayout,learning,financial,social,other;
    RecyclerView recyler;
    MainAdpter mainAdpter;
    private DatabaseHelper mDatabase;
    AdView adView;
    Adds adManager;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adManager = new Adds(this);
        lifebeautiful = findViewById(R.id.lifebeautifulcard);
         toolbaar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.activity_main);
        recyler = findViewById(R.id.recyler);
        setSupportActionBar(toolbaar);
        initNavigationDrawer();
        adView = findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        floatingActionButton=(FloatingActionButton)findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openCreateNote = new Intent(MainActivity.this, GoalsActivity.class);
                startActivity(openCreateNote);
               // adManager.showInterstitial();
            }
        });
        mainAdpter = new MainAdpter( this,list);
        recyler.setHasFixedSize(true);
        recyler.setLayoutManager(new LinearLayoutManager(this));

        mDatabase = new DatabaseHelper(this);
        list = mDatabase.listtime();

        if (list.size() > 0) {
            recyler.setVisibility(View.VISIBLE);
            mainAdpter = new MainAdpter(this, list);
            recyler.setAdapter(mainAdpter);
        } else {

            recyler.setVisibility(View.GONE);
            Toast.makeText(this, "There is no data", Toast.LENGTH_LONG).show();
        }


//
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//               this, drawer, toolbaar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//       drawer.addDrawerListener(toggle);
//        toggle.syncState();



//        NavigationView navigationView = (NavigationView) findViewById(R.id.nv);
//        navigationView.setNavigationItemSelectedListener(MainActivity.this);




    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                drawer.openDrawer(GravityCompat.START);
//                return true;
//        }

        if (actionBarDrawerToggle.onOptionsItemSelected(item)){

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }




    public void initNavigationDrawer() {

        NavigationView navigationView = (NavigationView)findViewById(R.id.nv);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();
//                if (id == R.id.nav_invite) {
//                    // Handle the camera action
//                } else if (id == R.id.track) {
//
//                    Intent intent=new Intent(MainActivity.this,DateActivity.class);
//                    startActivity(intent);
//                }
                if (id == R.id.share) {

                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "NewYear Resolution");
                        String shareMessage = "\nHi, Download this cool & fast performance newYearResolution App.\n";
//                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +"com.appzmachine.notificationmuter"+"\n\n";
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch (Exception e) {

                    }
                } else if (id == R.id.rateus) {

                    try{
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+getPackageName())));
                    }
                    catch (ActivityNotFoundException e){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName())));
                    }
                } else if (id == R.id.privacy) {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1LpNiZyaT105eVeshuL3niy_xejjNN7q2xDZp3zqx2Q4/edit"));
                    startActivity(browserIntent);
                } else if(id==R.id.more){
                   // https://play.google.com/store/apps/developer?id=Appz+machine
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Appz+machine"));
                    startActivity(intent);
                }
                drawer.closeDrawers();


//                switch (id){
//                    case R.id.home:
//                        Toast.makeText(getApplicationContext(),"Home",Toast.LENGTH_SHORT).show();
//                        drawer.closeDrawers();
//                        break;
//
//
//                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);

        drawer = findViewById(R.id.activity_main);

         actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawer,toolbaar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onRowClicked(String id, boolean b) {

    }
}