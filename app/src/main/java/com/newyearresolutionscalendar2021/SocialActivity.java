package com.newyearresolutionscalendar2021;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class SocialActivity extends AppCompatActivity {
  RecyclerView recyclerViewsocial;
    FinancialAdpter financialAdpter;
    LinearLayout assignlv;
    ImageView calenderimg, timer;
    EditText edttime, edtcalender, edttxt;
    Button save;
    Calendar calNow;
    int notificationid = 1;
    Context context;
    long time;
    SQLiteDatabase db;
    DatabaseHelper mDbHelper;
    // RecyclerView recyclerView;
    String goaltask, calenderdate;
    String caltime;
    Dialog settingsDialog;
    AdManager adManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
       // adManager=new AdManager(this);
        recyclerViewsocial=findViewById(R.id.recyclerViewsocial);
        assignlv=findViewById(R.id.assignlv);
        MyListData[] myListData1 = new MyListData[]{
                //new MyListData("Teach Childrens"),
                new MyListData("Donate"),

        };

        financialAdpter = new FinancialAdpter(myListData1, this);
        recyclerViewsocial.setHasFixedSize(true);
        recyclerViewsocial.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewsocial.setAdapter(financialAdpter);






        assignlv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsDialog = new Dialog(SocialActivity.this);
                LayoutInflater layoutInflater1 = LayoutInflater.from(SocialActivity.this);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                settingsDialog.setContentView(layoutInflater1.inflate(R.layout.dialogbox, null));
                settingsDialog.show();
                calenderimg = settingsDialog.findViewById(R.id.calenderimg);
                timer = settingsDialog.findViewById(R.id.timer);
                edttime = settingsDialog.findViewById(R.id.edttime);
                save = settingsDialog.findViewById(R.id.save);
                edtcalender = settingsDialog.findViewById(R.id.edtcalender);
                edttxt = settingsDialog.findViewById(R.id.edttxt);


                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goaltask = edttxt.getText().toString();
                        // Log.i("kkkk",goaltask);
                        mDbHelper = new DatabaseHelper(SocialActivity.this);
                        long checkinsertdata = mDbHelper.insertQuery(goaltask, calenderdate, caltime);
                        if (checkinsertdata != -1) {
                            notificationid=(int)checkinsertdata;
                            setAlarm(time);
                            //  Log.i("himani", String.valueOf(time));
                            Toast.makeText(SocialActivity.this, "set goal successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            // Toast.makeText(context, "New Entry no insertedt", Toast.LENGTH_SHORT).show();
                        }
                        settingsDialog.dismiss();

                       // adManager.showInterstitialAd(getApplicationContext());
                    }
                });


                calenderimg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toast.makeText(HealthActivity.this, "hello", Toast.LENGTH_SHORT).show();
                        Calendar mcurrentDate = Calendar.getInstance();
                        int mYear = mcurrentDate.get(Calendar.YEAR);
                        int mMonth = mcurrentDate.get(Calendar.MONTH);
                        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog mDatePicker = new DatePickerDialog(SocialActivity.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                // TODO Auto-generated method stub
                                /*      Your code   to get date and time   */
                                selectedmonth = selectedmonth + 1;
                                // eReminderDate.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);

                                edtcalender.setText(selectedday + "/" + selectedmonth + "/" + selectedyear);
                                calenderdate = edtcalender.getText().toString();


                            }
                        }, mYear, mMonth, mDay);
                        mDatePicker.setTitle("Select Date");
                        mDatePicker.show();
                    }
                });


                timer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(SocialActivity.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                calNow = Calendar.getInstance();
                                // Calendar calSet = (Calendar) calNow.clone();
                                calNow.set(Calendar.HOUR_OF_DAY, selectedHour);
                                calNow.set(Calendar.MINUTE, selectedMinute);
                                calNow.set(Calendar.SECOND, 0);
                                calNow.set(Calendar.MILLISECOND, 0);
                                time = calNow.getTimeInMillis();
                                if (calNow.compareTo(calNow) <= 0) {

                                    //Today Set time passed, count to tomorrow
                                    calNow.add(Calendar.DATE, 1);

                                }
                                //  edttime.setText(selectedHour+":"+selectedMinute+"");


                                if (selectedHour > 12) {
                                    edttime.setText(String.valueOf(selectedHour - 12) + ":" + (String.valueOf(selectedMinute) + "PM"));
                                } else if (selectedHour == 12) {
                                    edttime.setText("12" + ":" + (String.valueOf(selectedMinute) + "PM"));
                                } else if (selectedHour < 12) {
                                    if (selectedHour != 0) {
                                        edttime.setText(String.valueOf(selectedHour) + ":" + (String.valueOf(selectedMinute) + "AM"));
                                    } else {
                                        edttime.setText("12" + ":" + (String.valueOf(selectedMinute) + "AM"));
                                    }
                                }

                               // setAlarm(time);
                                caltime = edttime.getText().toString();

                            }
                        }, hour, minute, true);
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }


                });
            }
        });
    }
    private void setAlarm(long calNow) {
        Intent notificationIntent = new Intent(getApplicationContext(), MyReceiver.class);
        notificationIntent.putExtra("notificationId", notificationid);
        notificationIntent.putExtra("todo", edttxt.getText().toString());
        notificationIntent.putExtra("time", calNow);
        notificationIntent.putExtra("dates", calenderdate);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) getSystemService(getApplicationContext().ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calNow, 7 * 24 * 60 * 60 * 1000, pendingIntent);

       // adManager.showInterstitialAd(getApplicationContext());

    }
}