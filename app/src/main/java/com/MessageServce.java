package com;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.newyearresolutionscalendar2021.MainActivity;
import com.newyearresolutionscalendar2021.R;

public class MessageServce extends FirebaseMessagingService {


    private String NOTIFICATION_CHANNEL_ID = "100";
    private static RemoteViews contentView;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.i("himanisingh", String.valueOf(remoteMessage));

        showNotification(remoteMessage);

        // CustomNotification();
//        contentView = new RemoteViews(getPackageName(), R.layout.custom_notification);
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle("collapsed title")
//                .setContentText("collapsed text")
//                .setCustomBigContentView(contentView);
//
//        Notification notification = mBuilder.build();
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, notification);


    }


    public void showNotification(RemoteMessage remoteMessage) {
//
//        contentView = new RemoteViews(getPackageName(), R.layout.pushnotificationlayout);
//        contentView.setImageViewResource(R.id.image1, R.drawable.notificationblocker);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);
            //Configure Notification Channel
            notificationChannel.setDescription("Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);

            notificationManager.createNotificationChannel(notificationChannel);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(remoteMessage.getNotification().getTitle())
                    .setAutoCancel(true)
                    .setSound(defaultSound)
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setContentIntent(pendingIntent)
                    .setWhen(System.currentTimeMillis())
                    .setPriority(Notification.PRIORITY_MAX)
                    .setAutoCancel(true);


            notificationManager.notify(1, notificationBuilder.build());

        } else {

            NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(remoteMessage.getNotification().getTitle())
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);


//            Notification notification = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle("UnusedAppRemover")
//                    .setContentText(string)
//                    .setContentIntent(pendingIntent)
//                    .setAutoCancel(true)
//                    .build();
//            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification.build());


        }

    }

}
