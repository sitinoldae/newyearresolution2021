package com;

import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.iid.InstanceIdResult;

public class FirebaseInstentId extends FirebaseInstanceIdService {


    private static final String TAG = FirebaseInstanceId.class.getSimpleName();

//    @Override
//    public void onTokenRefresh() {
//        super.onTokenRefresh();

    private static String refreshedToken;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        String token=FirebaseInstanceId.getInstance().getToken();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Log.i("ghk",token);
                // send it to server
            }
        });

    }

    public static String getFirebaseId() {

        return  refreshedToken;

    }

}
